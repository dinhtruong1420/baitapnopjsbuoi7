var numberArray = [];

function themSo() {
	var inputEle = document.getElementById("txt-number");
	if (inputEle.value == "") {
		return;
	}
	var number = inputEle.value * 1;
	numberArray.push(number);
	inputEle.value = "";
	console.log('numberArray: ', numberArray);
	var contentHtml = '';
	contentHtml = `<p>Array hiện tại:${numberArray}</p>`;
	document.querySelector("#txtArray").innerHTML = contentHtml;
}
document.querySelector('#btnTinhTong').addEventListener("click", function possitiveSum() {
	var content = "";
	var sum = 0;
	for (var i = 0; i <= numberArray.length; i++) {
		if (numberArray[i] > 0) {
			sum += numberArray[i];
			console.log(sum);
		}
		content = `<p>Tổng array:${sum}</p>`;
		document.getElementById("txt-tinhtong").innerHTML = content;
	}
})
document.querySelector("#btnDemSo").addEventListener("click", function countPossitiveNum() {
	var possitiveArray = numberArray.filter(function (n) {
		return n > 0
	})
	var possitiveQuantity = possitiveArray.length;
	document.getElementById("txt-demsoduong").innerHTML = `Số lượng số dương: ${ possitiveQuantity}`;
})
document.querySelector("#btnMinNum").addEventListener("click", function findMinNum() {
	var minNum = numberArray[0];

	for (var i = 0; i < numberArray.length; i++) {
		if (numberArray[i] < minNum) {
			minNum = numberArray[i];
		}
	}
	document.getElementById("txt-minnum").innerHTML = `So nho nhat: ${minNum}`;
})
document.querySelector("#btnMinPositive").addEventListener("click", function findMinPositive() {
	var positiveArray = numberArray.filter(function (n) {
		return n > 0
	});
	var minPositive = positiveArray[0];
	for (var i = 0; i < positiveArray.length; i++) {
		if (minPositive > positiveArray[i]) {
			minPositive = positiveArray[i];
		}
	}
	document.getElementById("txt-minpositive").innerHTML = `<p>Số dương nhỏ nhất ${minPositive}</p>`;
})
document.querySelector("#btnEvenNum").addEventListener("click", function findLastEvenNum() {
	var lastEvenNum;
	for (var i = 0; i < numberArray.length; i++) {
		if (numberArray[i] % 2 == 0) {
			lastEvenNum = numberArray[i];
		}
	}
	document.getElementById("txt-evennum").innerHTML = `<p>Số chẵn cuối cùng: ${lastEvenNum}</p>`;
})
document.querySelector("#btnSwap").addEventListener("click", function swapTwoNum() {
	var firstNum = document.getElementById("firstnum").value * 1;
	var secondNum = document.getElementById("secondnum").value * 1;
	console.log('numberArray[firstNum]: ', numberArray[firstNum]);
	console.log('numberArray[secondNum]: ', numberArray[secondNum]);
	var temp = numberArray[firstNum];
	numberArray[firstNum] = numberArray[secondNum];
	numberArray[secondNum] = temp;
	console.log('numberArray: ', numberArray);
	document.getElementById("txt-swapnum").innerHTML = `<p>Mảng sau khi đổi:${numberArray} </p>`
})
document.querySelector("#btnSort").addEventListener("click", function sortAscending() {
	for (var i = 0; i < numberArray.length; i++) {
		for (var j = 0; j < numberArray.length - i - 1; j++) {
			if (numberArray[j] > numberArray[j + 1]) {
				var temp = numberArray[j];
				numberArray[j] = numberArray[j + 1];
				numberArray[j + 1] = temp;
			}
		}
	}
	console.log(numberArray);
	document.getElementById("txt-sort").innerHTML = `<p>Mảng sau khi sắp xếp: ${numberArray}</p>`;
})

function isPrime(number) {
	if (number == 1 || number < 1) {
		return false;
	}
	for (var i = 2; i < number; i++) {
		if (number % i == 0) {
			return false;
		}
	}
	return true;
}
document.querySelector("#btnPrime").addEventListener("click", function findFirstPrime() {
	// var firstPrime=findPrime()
	for (var i = 0; i < numberArray.length; i++) {
		if (isPrime(numberArray[i])) {
			console.log('numberArray[i]): ', numberArray[i]);
			document.getElementById("txt-prime").innerHTML = `<p>Số nguyên tố đầu tiên: ${numberArray[i]}</p>`;
			break;
		}
	}
})
document.querySelector("#btnInteger").addEventListener("click", function countInteger() {
	var count = 0;
	for (var i = 0; i < numberArray.length; i++) {
		if (Number.isInteger(numberArray[i])) {
			count++;
		}
	}
	document.getElementById("txt-integer").innerHTML = `<p>Số nguyên: ${count}</p>`;
})
document.querySelector("#btnCompare").addEventListener("click", function comparePositiveNegative() {
	// var positiveArray = numberArray.filter(function (number) {
	// 	return number > 0;
	// });
	// console.log('positiveArray: ', positiveArray);
	// var negativeArray = numberArray.filter(function (n) {
	// 	return n < 0;
	// });
	// console.log('positiveArray: ', positiveArray.length);
	// contentHtml = '';
	// positiveArray.length > negativeArray.length ? contentHtml = `<p>Số dương>số âm</p>` : contentHtml = `<p>Số dương<Số âm</p>`;
	// document.getElementById("txt-compare").innerHTML = contentHtml;
	var negativeQuantity = 0;
	var positiveQuantity = 0;
	for (var i = 0; i < numberArray.length; i++) {
		if (numberArray[i] > 0) {
			positiveQuantity++;
		} else if (numberArray[i] < 0) {
			negativeQuantity++;
		}
	}
	var contentHtml = "";
	if (positiveQuantity > negativeQuantity) {
		contentHtml = `<p>số dương > số âm</p>`
		document.getElementById("txt-compare").innerHTML = contentHtml;
	} else if (positiveQuantity < negativeQuantity) {
		contentHtml = `<p>Số dương < số âm <p>`;
		document.getElementById("txt-compare").innerHTML = contentHtml;
	}
})